# Algoritmo de Compressão LZ78 

## Instruções

Projeto desenvolvido na IDE Intellij

### Linha de Comando

#### Compilar o projeto

- Entre na pasta do projeto pelo terminal
- Execute os comandos:

```sh
javac -d . src/lca/**/*.java
jar cvmf META-INF/MANIFEST.MF ed2.jar lca
```

> Caso apareça a mensagem abaixo apenas ignore

```
Note: Some input files use unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
```

#### Executar

- Após ter compilado o projeto, ainda dentro da pasta do projeto, pelo terminal execute:

```sh
java -jar ed2.jar
```

- Irá abrir uma interface solicitando um arquivo com a palavra a ser codificada.

> Um arquivo `input-demo.txt` está disponível para ser utilizado
