package lca.utils;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class IOFile {
	/**
	 * Obtém um arquivo através de uma interface gráfica
	 *
	 * @return arquivo selecionado pelo usuário
	 * @throws FileNotFoundException
	 */
	public static File getFileFromDialog() throws FileNotFoundException {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int result = fileChooser.showOpenDialog(null);

		if (result != JFileChooser.APPROVE_OPTION) {
			throw new FileNotFoundException();
		}

		return fileChooser.getSelectedFile();
	}

	public static ArrayList<String> getRecordsFromFile() throws FileNotFoundException {
		File inputFile;
		System.out.printf("Por favor, selecione o arquivo a ser lido:\n");

		inputFile = IOFile.getFileFromDialog();
		System.out.printf("Lendo arquivo %s...\n\n", inputFile.getName());
		return IOFile.readFile(inputFile.getAbsolutePath());
	}

	public static ArrayList<String> readFile(String path) throws FileNotFoundException {
		ArrayList<String> lines = new ArrayList<>();
		final Scanner scanner = new Scanner(new File(path));

		while (scanner.hasNext()) {
			lines.add(scanner.nextLine());
		}

		scanner.close();

		return lines;
	}

	public static void writeDataInFile(String outputFile, String data, boolean isAppend) throws IOException {
		File file = new File(outputFile);

		if (!file.exists()) {
			file.createNewFile();
			isAppend = false;
		}
		FileWriter fileWriter = new FileWriter(file, isAppend);
		PrintWriter printWriter = new PrintWriter(fileWriter);

		printWriter.printf(data);
		printWriter.printf(System.lineSeparator() + System.lineSeparator());
		printWriter.close();
	}
}
