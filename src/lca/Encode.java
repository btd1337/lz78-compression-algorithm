package lca;

import java.util.ArrayList;
import java.util.List;

public class Encode {
	private String code;
	private int num;
	private char character;
	private String tmp;
	private final List<Encode> tags = new ArrayList<>();
	private final List<String> dict = new ArrayList<String>();

	Encode() {
		this.code = "";
	}


	Encode(String code) {
		this.code = code;
		this.execute();
	}

	private Encode(int num, char character) {
		this.num = num;
		this.character = character;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String revealCode() {
		return this.code;
	}

	public void execute() {
		boolean isCheck = true;
		for (int i = 0; i < code.length(); i++) {
			boolean isCheckAux = false;
			tmp = Character.toString(code.charAt(i));
			for (int j = dict.size() - 1; j >= 0; j--) {
				if (dict.get(j).equals(tmp)) {
					isCheckAux = true;
					num = j + 1;
					i++;
					if (i == code.length()) {
						isCheck = false;
						character = code.charAt(code.length() - 1);
						break;
					}
					tmp = tmp + code.charAt(i);
					j = dict.size();
				}
			}
			if (isCheckAux == false || isCheck == false)
				num = 0;
			if (isCheck != false)
				character = code.charAt(i);
			if (isCheck == true)
				dict.add(tmp);
			tags.add(new Encode(num, character));
		}
	}

	public void printDictionary() {
		for (int i = 0; i < dict.size(); i++) {
			System.out.println(i + 1 + " " + dict.get(i));
		}
	}

	void printTags() {
		for (int i = 0; i < tags.size(); i++) {
			System.out.printf("< %d , %c > \n", tags.get(i).num, tags.get(i).character);
		}
	}
}
