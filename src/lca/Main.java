package lca;

import lca.utils.IOFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		ArrayList<String> records;
		int option;
		Scanner scanner = new Scanner(System.in);

		try {
			records = IOFile.getRecordsFromFile();
		} catch (FileNotFoundException e) {
			System.out.println("Ocorreu um erro na leitura do arquivo");
			e.printStackTrace();
			return;
		}

		for (String line :
				records) {
			System.out.println(line);
		}

		do {
			System.out.printf("\nSelecione uma opção:\n1-Codificar\n2-Decodificar\n0-Sair\n");
			option = scanner.nextInt();

			switch (option) {
				case 1: encode(records); break;
				case 2: decode(); break;
				case 0:
					System.out.println("Saindo...");
					return;
				default:
					System.out.println("Valor inválido");
			}
		} while (option != 1 || option != 2);
	}

	public static void encode(ArrayList<String> records) {
		System.out.println("Encoding");
		int lineToGetCode = 0;

		// apenas a primeira linha por enquanto...
		Encode encode = new Encode(records.get(lineToGetCode));
		encode.printDictionary();
		encode.printTags();
	}

	public static void decode () {
		System.out.println("Decoding");

		Decode decode = new Decode();
		decode.getTags();
		decode.execute();

		System.out.println(decode.getOriginalCode());
		decode.printDictionary();
	}
}
