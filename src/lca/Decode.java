package lca;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Decode {
	private String originalCode;
	private int num;
	private String character;
	private char lastChar;
	private final List<Decode> tags = new ArrayList<>();
	private final List<String> dict = new ArrayList<String>();

	private Decode(int num, String character) {
		this.num = num;
		this.character = character;
	}

	public Decode() {
		this.originalCode = "";
		this.lastChar = ' ';
	}

	public String execute() {
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).num == 0 && i + 1 != tags.size()) {
				dict.add(tags.get(i).character);
			} else if (tags.get(i).num != 0) {
				dict.add(dict.get(tags.get(i).num - 1) + tags.get(i).character);
			} else if (tags.get(i).num == 0 && i + 1 == tags.size())
				this.lastChar = tags.get(i).character.charAt(0);
		}
		for (int i = 0; i <= dict.size(); i++) {
			if (i == dict.size()) {
				this.originalCode += this.lastChar;
				break;
			}
			this.originalCode += dict.get(i);
		}
		return originalCode;
	}

	public String getOriginalCode() {
		return this.originalCode;
	}

	public void printDictionary() {
		for (int i = 0; i < dict.size(); i++) {
			System.out.println(i + 1 + " " + dict.get(i));
		}
	}

	public void getTags() {
		Scanner in = new Scanner(System.in);
		System.out.println("Informe o número de tags : ");
		int x = in.nextInt();
		for (int i = 0; i < x; i++) {
			System.out.println("Informe o valor: ");
			num = in.nextInt();
			System.out.println("Informe o caractere: ");
			character = in.next();
			tags.add(new Decode(num, character));
		}
		System.out.println();
	}
}
